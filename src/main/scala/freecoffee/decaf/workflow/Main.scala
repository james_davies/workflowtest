package freecoffee.decaf.workflow

import freecoffee.decaf.workflow.dispatch.{Accept, Send, Decline, JobDispatchWorkflow}
import freecoffee.decaf.workflow.dispatch.rest._

object Main extends App {

  val service = new WorkflowService
  val controller = new WorkflowController(service)

  def runDynamic = {

    val wfName = "JobDispatch"
    val SEND = "\"Send\""
    val ACCEPT = "\"Accept\""

    val channel = controller.createDynamicChannel(wfName)
    println(controller.getState(wfName, channel))

    controller.sendDynamicMessage(wfName, channel, SEND)
    println(controller.getState(wfName, channel))

    controller.sendDynamicMessage(wfName, channel, ACCEPT)
    println(controller.getState(wfName, channel))

  }

  def runStatic = {
    val channel = service.createChannel(JobDispatchWorkflow)
    println(service.getState(JobDispatchWorkflow)(channel))

    service.sendMessage(JobDispatchWorkflow)(channel, Send)
    println(service.getState(JobDispatchWorkflow)(channel))

    service.sendMessage(JobDispatchWorkflow)(channel, Accept)
    println(service.getState(JobDispatchWorkflow)(channel))
  }

  def runDependent = {
    val pending = JobDispatchWorkflow.defaultState
    println(pending)

    val sent    = Send(pending)
    println(sent)

    val accepted = Accept(sent)
    println(accepted)

    // Does not compile - type error
    // val declined = Decline(accepted)
  }

  println("Dynamic: ")
  runDynamic

  println("\n\nStatic: ")
  runStatic

  println("\n\nDependent:")
  runDependent





}