package freecoffee.decaf.workflow.dispatch

import freecoffee.decaf.workflow.WorkflowTransition

sealed trait JobDispatchWorkflowTransition extends WorkflowTransition

case object Send extends JobDispatchWorkflowTransition {
  type FromState   = PendingDispatch.type
  type TargetState = Sent.type

  def apply(state:FromState):TargetState = Sent
}

case object Accept extends JobDispatchWorkflowTransition {
  type FromState   = Sent.type
  type TargetState = Accepted.type

  def apply(state:FromState):TargetState = Accepted
}

case object Decline extends JobDispatchWorkflowTransition {
  type FromState   = Sent.type
  type TargetState = Declined.type

  def apply(state:FromState):TargetState = Declined
}

