package freecoffee.decaf.workflow.dispatch

import argonaut._, Argonaut._

package object rest {
  implicit def dispatchTransitionCodec : CodecJson[JobDispatchWorkflowTransition] = {
    val encoder = new EncodeJson[JobDispatchWorkflowTransition] {
      override def encode(a: JobDispatchWorkflowTransition): Json = a.toString.asJson
    }

    val decoder = new DecodeJson[JobDispatchWorkflowTransition] {
      override def decode(c: HCursor): DecodeResult[JobDispatchWorkflowTransition] = c.focus.stringOrEmpty match {
        case "Send" => DecodeResult.ok(Send)
        case "Accept" => DecodeResult.ok(Accept)
        case "Decline" => DecodeResult.ok(Decline)
        case s => DecodeResult.fail("Invalid transition: " + s, c.history)
      }
    }

    CodecJson(encoder.encode, decoder.decode)
  }

  implicit def dispatchStateCodec : CodecJson[JobDispatchWorkflowState] = {
    val encoder = new EncodeJson[JobDispatchWorkflowState] {
      override def encode(a: JobDispatchWorkflowState): Json = a.toString.asJson
    }

    val decoder = new DecodeJson[JobDispatchWorkflowState] {
      override def decode(c: HCursor): DecodeResult[JobDispatchWorkflowState] = c.focus.stringOrEmpty match {
        case "PendingDispatch" => DecodeResult.ok(PendingDispatch)
        case "Sent"            => DecodeResult.ok(Sent)
        case "Accepted"        => DecodeResult.ok(Accepted)
        case "Declined"        => DecodeResult.ok(Declined)
      }
    }

    CodecJson(encoder.encode, decoder.decode)
  }


}
