package freecoffee.decaf.workflow.dispatch

import scalaz.syntax.std.option._

import freecoffee.decaf.workflow.Workflow

object JobDispatchWorkflow extends Workflow {
  type State      = JobDispatchWorkflowState
  type Transition = JobDispatchWorkflowTransition


  override def defaultState = PendingDispatch

  override def process(state: JobDispatchWorkflowState, transition: JobDispatchWorkflowTransition): Option[State] = {
    (state, transition) match {
      case (PendingDispatch, Send) => Send(PendingDispatch).some
      case (Sent, Accept)          => Accept(Sent).some
      case (Sent, Decline)         => Decline(Sent).some
      case _ => None
    }
  }
}
