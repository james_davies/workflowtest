package freecoffee.decaf.workflow

trait Workflow {
  type State      <: WorkflowState
  type Transition <: WorkflowTransition

  def defaultState:State
  def process(state:State, transition: Transition):Option[State]
}