package freecoffee.decaf.workflow

import argonaut._, Argonaut._
import freecoffee.decaf.workflow.dispatch.JobDispatchWorkflow
import freecoffee.decaf.workflow.dispatch.rest._

import scala.collection.mutable.Map
import scalaz.{\/-, -\/, \/}

class WorkflowController(service:WorkflowService) {
  val workflows : Map[String, WorkflowInfo] = Map("JobDispatch" -> WorkflowInfo.box(JobDispatchWorkflow))

  def getOrPanic[A,B](disj: A \/ B):B = disj match {
    case -\/(failure) => throw new RuntimeException(failure.toString)
    case \/-(success)  => success
  }

  def createDynamicChannel(workflowName:String):ChannelId = {
    val box = workflows(workflowName)
    implicit def scodec : CodecJson[box.workflow.State]      = box.scodec

    service.createChannel(box.workflow)
  }

  def sendDynamicMessage(workflowName: String, channelId: ChannelId, transitionBlob: String): Unit = {
    val box = workflows(workflowName)
    implicit def tcodec : CodecJson[box.workflow.Transition] = box.tcodec
    implicit def scodec : CodecJson[box.workflow.State]      = box.scodec

    val trans = getOrPanic(transitionBlob.decodeEither[box.workflow.Transition])

    service.sendMessage(box.workflow)(channelId, trans)
  }

  def getState(workflowName:String, channelId: ChannelId):Workflow#State = {
    val box = workflows(workflowName)
    implicit def scodec : CodecJson[box.workflow.State]      = box.scodec
    service.getState(box.workflow)(channelId)
  }
}