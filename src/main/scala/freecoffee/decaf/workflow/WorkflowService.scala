package freecoffee.decaf.workflow

import java.util.UUID

import argonaut.Argonaut._
import argonaut.{CodecJson, _}

import scala.collection.mutable._


case class ChannelId(id:String) extends AnyVal

class WorkflowService {
  def panic(msg:String) = new RuntimeException(msg)

  val state : Map[ChannelId, Json] = Map.empty

  def createChannel(workflow: Workflow)(implicit sc:CodecJson[workflow.State]):ChannelId = {
    val channel = ChannelId(UUID.randomUUID().toString)
    val state   = workflow.defaultState
    saveState(workflow)(channel, state)

    channel
  }

  def getState(workflow: Workflow)(channelId: ChannelId)(implicit sc:CodecJson[workflow.State]): workflow.State = {
    state(channelId).as[workflow.State].toOption.get
  }

  def saveState(workflow:Workflow)(channelId: ChannelId, newState:workflow.State)(implicit ev:CodecJson[workflow.State]):Unit = {
    state(channelId) = newState.asJson
  }

  def sendMessage(workflow:Workflow)(channelId: ChannelId, transition:workflow.Transition)(implicit ev:CodecJson[workflow.State]):Unit = {
    val state = getState(workflow)(channelId)
    val newState = workflow.process(state, transition).getOrElse(throw new RuntimeException("Invalid Transition: " + transition + " for state " + state))
    saveState(workflow)(channelId, newState)
  }
}
