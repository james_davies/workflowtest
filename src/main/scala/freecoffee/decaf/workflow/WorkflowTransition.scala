package freecoffee.decaf.workflow

trait WorkflowTransition {
  type FromState
  type TargetState

  def apply(state:FromState):TargetState
}
