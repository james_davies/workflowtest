package freecoffee.decaf.workflow

import argonaut.CodecJson

trait WorkflowInfo {
  val workflow:Workflow
  val tcodec:CodecJson[workflow.Transition]
  val scodec:CodecJson[workflow.State]

  val wf     : workflow.type = workflow
//  val statee : workflow.State = ???
//  val transs : workflow.Transition = ???
}

object WorkflowInfo {
  def box(eworkflow: Workflow)(implicit etcodec:CodecJson[eworkflow.Transition], escodec:CodecJson[eworkflow.State]):WorkflowInfo = {
        new WorkflowInfo {
          override val tcodec: CodecJson[this.workflow.Transition] = etcodec
          override val scodec: CodecJson[this.workflow.State] = escodec
          override val workflow: eworkflow.type = eworkflow
        }
      }
}