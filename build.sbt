name := "CoffeeBet"

version := "1.0"

scalaVersion := "2.11.7"


libraryDependencies += "org.scalaz" %% "scalaz-core" % "7.1.5"

libraryDependencies += "org.scalaz" %% "scalaz-concurrent" % "7.1.5"

libraryDependencies += "io.argonaut" %% "argonaut" % "6.1"